import axios from "axios"

const { hostname, protocol, port } = window.location
const baseUrl = `${protocol}//${hostname}:${port}/${process.env.REACT_APP_API_NAME}`

export class FetchData {

  static async get<T = undefined> (path: string): Promise<T> {

    try {

      const res = await axios.get(`${baseUrl}/${path}`)
      return res.data

    }
    catch (error) {

      throw new Error("Erreur communication avec l'api")

    }

  }

  static async post<T = undefined, Y = undefined> (path: string, data: Y): Promise<T> {

    try {

      const res = await axios.post(`${baseUrl}/${path}`, data)
      return res.data

    }
    catch (error) {

      throw new Error("Erreur communication avec l'api")

    }

  }

}
