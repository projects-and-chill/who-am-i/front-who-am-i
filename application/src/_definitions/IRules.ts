export interface IRules {
  wordQty: number
  time: ["stopwatch" | "countdown", number]
  endGame: ["rounds" | "points", number]
  podium: "ascending" | "descending"
}
