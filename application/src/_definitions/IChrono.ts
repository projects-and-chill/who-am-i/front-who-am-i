import { IRules } from "./IRules"

export interface IChrono {
  id: string
  roomId :string
  defaultTime: number
  type: IRules["time"][0]
  state: "play" | "pause"
  startAt: number
  milliseconds: number
}
