export interface ITeam {
  id: string
  playOrder: number
  played: boolean
  wordId: string
  color: string
  score: number
  roomId: string
  guessedWords: string[]
}
