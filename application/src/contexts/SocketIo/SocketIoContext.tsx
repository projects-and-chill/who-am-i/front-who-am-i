import React, { createContext, FC, ReactNode, useContext } from "react"
import { io, Socket } from "socket.io-client"

const socket = io().connect()

export interface ISocketIoContext {
  socket: Socket,
}

type SocketIoProps = { children?: ReactNode }
const SocketIoContext = createContext<ISocketIoContext | null>(null)

const SocketIoProvider: FC<SocketIoProps> = ({ children }) => {

  return (
    <SocketIoContext.Provider value={{ socket }}>
      {children}
    </SocketIoContext.Provider>
  )

}

function useSocketIo () {

  const socketContext = useContext(SocketIoContext)
  if (socketContext === undefined)

    throw new Error("useSocket must be used within a SocketioProvider")

  return socketContext as ISocketIoContext

}

export { SocketIoProvider, useSocketIo }
