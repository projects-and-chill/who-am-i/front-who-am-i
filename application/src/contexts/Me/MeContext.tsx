import React, { createContext, Dispatch, FC, ReactNode, SetStateAction, useContext, useMemo, useState } from "react"
import { IMe } from "../../_definitions/IMe"

type IMeContext = {
  me: IMe
  setMe: Dispatch<SetStateAction<IMe>> & Dispatch<IMe>
}
type Props = { children?: ReactNode }

const MeContext = createContext<IMeContext | null>(null)

const meTemplate: IMe = {
  id: "",
  username: "",
  blinded: false,
  leader: false,
  timeMaster: false,
  roomId: "",
  teamId: "",
  vote: 0,
  avatar: ""
}

const MeProvider: FC<Props> = ({ children }) => {

  const [me, setMe] = useState<IMe>(meTemplate)

  const value: IMeContext = useMemo(() => ({ me, setMe }), [me])

  return <MeContext.Provider value={value}>{children}</MeContext.Provider>

}

function useMe () {

  const meContext = useContext(MeContext)
  if (!meContext)

    throw new Error("useMe must be used within a MeProvider")

  return meContext as IMeContext

}

export { meTemplate, MeProvider, useMe }
