import React, {
  createContext,
  FC,
  ReactNode,
  useContext,
  useMemo,
  useReducer
} from "react"
import { TeamReducer } from "./TeamsReducer"
import { TeamsActions } from "./TeamsActions"
import { ITeam } from "../../_definitions/ITeam"

type Props = { children?: ReactNode }
type State = ITeam[]
type Action = {
  type: TeamsActions
  payload?: unknown
}
type Dispatch = (action: Action) => void
type TeamsContext = {
  teams: State
  setTeams: Dispatch
}

const TeamsContext = createContext<TeamsContext | null>(null)

const TeamsProvider: FC<Props> = ({ children }) => {

  const [teams, setTeams] = useReducer(TeamReducer, [])

  const value: TeamsContext = useMemo(() => ({ teams, setTeams }), [teams])

  return <TeamsContext.Provider value={value}>{children}</TeamsContext.Provider>

}

function useTeams () {

  const teamsContext = useContext(TeamsContext)
  if (!teamsContext)

    throw new Error("useTeams must be used within a TeamProvider")

  return teamsContext as TeamsContext

}

export { TeamsProvider, useTeams }
export type { Action, State }
