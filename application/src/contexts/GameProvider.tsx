import React, { FC, ReactNode } from "react"
import { RoomProvider } from "./Room/RoomContext"
import { ScoresProvider } from "./Scores/ScoresContext"
import { PlayersProvider } from "./Players/PlayersContext"
import { SocketIoProvider } from "./SocketIo/SocketIoContext"
import { MeProvider } from "./Me/MeContext"
import { RulesProvider } from "./Rules/RulesContext"
import { TeamsProvider } from "./Teams/TeamsContext"
import { ChronoProvider } from "./Chrono/ChronoContext"

type GameProps = { children?: ReactNode }

const GameProvider: FC<GameProps> = ({ children }) => {

  return (
    <SocketIoProvider>
      <RoomProvider>
        <TeamsProvider>
          <PlayersProvider>
            <RulesProvider>
              <ScoresProvider>
                <ChronoProvider>
                  <MeProvider>
                    {children}
                  </MeProvider>
                </ChronoProvider>
              </ScoresProvider>
            </RulesProvider>
          </PlayersProvider>
        </TeamsProvider>
      </RoomProvider>
    </SocketIoProvider>
  )

}

export { GameProvider }
