import React, {
  createContext,
  Dispatch,
  FC,
  ReactNode,
  SetStateAction,
  useContext,
  useMemo,
  useState
} from "react"
import { IRules } from "../../_definitions/IRules"

type IRulesContext = {
  rules: IRules
  setRules: Dispatch<SetStateAction<IRules>> & Dispatch<IRules>
}
type Props = { children?: ReactNode }

const RulesContext = createContext<IRulesContext | null>(null)

const rulesTemplate: IRules = {
  endGame: ["rounds", 5],
  time: ["countdown", 45],
  wordQty: 3,
  podium: "ascending"
}

const RulesProvider: FC<Props> = ({ children }) => {

  const [rules, setRules] = useState<IRules>(rulesTemplate)

  const value: IRulesContext = useMemo(() => ({ rules, setRules }), [rules])

  return <RulesContext.Provider value={value}>{children}</RulesContext.Provider>

}

function useRules () {

  const rulesContext = useContext(RulesContext)
  if (!rulesContext)

    throw new Error("useRules must be used within a RulesProvider")

  return rulesContext as IRulesContext

}

export { rulesTemplate, RulesProvider, useRules }
