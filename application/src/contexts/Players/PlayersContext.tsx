import React, {
  createContext,
  FC,
  ReactNode,
  useContext,
  useMemo,
  useReducer
} from "react"
import { PlayerReducer } from "./PlayersReducer"
import { PlayerActions } from "./PlayerActions"
import { IPlayer } from "../../_definitions/IPlayer"

type Props = { children?: ReactNode }
type State = IPlayer[]
type Action = {
  type: PlayerActions
  payload?: unknown
}
type Dispatch = (action: Action) => void
type PlayersContext = {
  players: State
  setPlayers: Dispatch
}

const PlayersContext = createContext<PlayersContext | null>(null)

const PlayersProvider: FC<Props> = ({ children }) => {

  const [players, setPlayers] = useReducer(PlayerReducer, [])

  const value: PlayersContext = useMemo(
    () => ({ players, setPlayers }),
    [players]
  )

  return (
    <PlayersContext.Provider value={value}>{children}</PlayersContext.Provider>
  )

}

function usePlayers () {

  const playersContext = useContext(PlayersContext)
  if (!playersContext)

    throw new Error("usePlayers must be used within a PlayerProvider")

  return playersContext as PlayersContext

}

export { PlayersProvider, usePlayers }
export type { Action, State }
