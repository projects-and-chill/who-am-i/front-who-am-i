export enum PlayerActions {
  RESET = "RESET",
  ADD = "ADD",
  REMOVE = "REMOVE",
  UPDATE = "UPDATE",
  ALL = "ALL"
}
