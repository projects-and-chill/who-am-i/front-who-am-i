import React from "react"
import { Navigate, Route, Routes as Switch } from "react-router-dom"
import Tests from "./components/Tests/Tests"
import GameOutlet from "./components/GameOutlet/GameOutlet"
import { GameProvider } from "./contexts/GameProvider"
import Room from "./components/GameOutlet/Room/Room"
import Lobby from "./components/GameOutlet/Lobby/Lobby"

function Router () {

  return (
    <Switch>
      <Route
        path='game'
        element={
          <GameProvider>
            <GameOutlet />
          </GameProvider>
        }
      >
        <Route index element={<Navigate to='lobby' replace />} />
        <Route path='lobby' element={<Lobby />} />
        <Route path='play' element={<Room />} />
      </Route>
      <Route path='tests' element={<Tests />} />
      <Route path='*' element={<Navigate to='/game' replace />} />
    </Switch>
  )

}

export { Router }
