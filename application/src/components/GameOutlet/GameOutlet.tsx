import React, { useEffect } from "react"
import { Outlet } from "react-router-dom"
import { message } from "antd"
import { useSocketIo } from "../../contexts/SocketIo/SocketIoContext"
import { useFirstConnection } from "../../contexts/SocketIo/FirstConnectionContext"

function GameOutlet () {

  const { socket } = useSocketIo()
  const { setFirstConnection } = useFirstConnection()
  useEffect(() => {

    socket
      .on("connect", () => {
        message.info("Bienvenue")
      })
      .on("message", (type: "info" | "error" | "warning", msg: string) => {
        message[type](msg)
      })
      .on("disconnect", () => {
        message.info("Au revoir")
      })

    return () => {
      setFirstConnection(false)
      socket
        .off("connect")
        .off("disconnect")
        .off("message")
        .disconnect()
    }

  }, [setFirstConnection, socket])

  return (
    <div>
      <Outlet />
    </div>
  )

}

export default GameOutlet
