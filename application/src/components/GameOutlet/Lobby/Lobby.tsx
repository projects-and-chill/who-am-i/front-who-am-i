import React, { ChangeEvent, useCallback, useEffect, useState } from "react"
import { Card, Col, Input, Layout, message, Row } from "antd"
import JoinRoom from "./JoinRoom/JoinRoom"
import { useSocketIo } from "../../../contexts/SocketIo/SocketIoContext"
import { UserOutlined } from "@ant-design/icons"
import CreateRoom from "./CreateRoom/CreateRoom"
import { IRoom } from "../../../_definitions/IRoom"
import { roomTemplate, useRoom } from "../../../contexts/Room/RoomContext"
import { useTeams } from "../../../contexts/Teams/TeamsContext"
import { usePlayers } from "../../../contexts/Players/PlayersContext"
import { useNavigate } from "react-router-dom"
import { ITeam } from "../../../_definitions/ITeam"
import { IPlayer } from "../../../_definitions/IPlayer"
import { resetAllTeams, updateAllTeams } from "../../../contexts/Teams/TeamsActionCreator"
import { resetAllPlayers, updateAllPlayers } from "../../../contexts/Players/PlayersActionCreator"
import { MeServices } from "../../../contexts/Me/fetchMe"
import { meTemplate, useMe } from "../../../contexts/Me/MeContext"
import { playerEvents, roomEvents } from "../../../_definitions/Events"
import { rulesTemplate, useRules } from "../../../contexts/Rules/RulesContext"
import { ScoreHelper } from "../../../contexts/Scores/ScoreHelper"
import { useScores } from "../../../contexts/Scores/ScoresContext"
import { addScore, resetAllScores, updateAllScores } from "../../../contexts/Scores/ScoresActionCreator"
import { useFirstConnection } from "../../../contexts/SocketIo/FirstConnectionContext"

function Lobby () {

  const navigate = useNavigate()
  const { socket } = useSocketIo()
  const { setRoom } = useRoom()
  const { firstConnection } = useFirstConnection()
  const { setTeams } = useTeams()
  const { setRules } = useRules()
  const { setScores } = useScores()
  const { setPlayers } = usePlayers()
  const { setMe, me } = useMe()
  const [username, setUsername] = useState<string>()

  useEffect(() => {

    if (!firstConnection || me.username)
      socket.connect()

    if (me.username){
      setUsername(me.username)
      setMe(meTemplate)
      setRoom(roomTemplate)
      setRules(rulesTemplate)
      setPlayers(resetAllPlayers())
      setScores(resetAllScores())
      setTeams(resetAllTeams())
    }

  }, [firstConnection, me.username, setMe, setPlayers, setRoom, setRules, setScores, setTeams, socket])

  const handleUsername = (e: ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value)
  }

  const roomCallback = useCallback(
    (room: IRoom, teams: ITeam | ITeam[], players: IPlayer | IPlayer[]) => {

      setRoom(room)
      if (Array.isArray(teams)){
        setTeams(updateAllTeams(teams))
        const scores = ScoreHelper.formatMany(teams)
        setScores(updateAllScores(scores))
      }
      else {
        setTeams(updateAllTeams([teams]))
        const score = ScoreHelper.formatOne(teams)
        setScores(addScore(score))
      }

      Array.isArray(players)
        ? setPlayers(updateAllPlayers(players))
        : setPlayers(updateAllPlayers([players]))
      MeServices.fetchMe(socket.id, players, setMe, room)
      navigate("../play", { replace: false })

    },
    [navigate, setMe, setPlayers, setRoom, setScores, setTeams, socket.id]
  )

  const handleJoinRoom = useCallback(
    (roomId: string) => {

      if (!username)
        return message.warning("You should insert username")

      socket.emit(playerEvents.client.JOIN, ...[roomId, username], roomCallback)

    },
    [roomCallback, socket, username]
  )

  const handleCreateRoom = useCallback(
    (roomName: string) => {
      if (!username)
        return message.warning("You should insert username")
      else if (!roomName)
        return message.warning("You should insert room name")
      socket.emit(
        roomEvents.client.CREATE,
        { roomName, username },
        roomCallback
      )

    },
    [roomCallback, socket, username]
  )
  const colSpan = { xs: 24, sm: 22, md: 18, lg: 14, xl: 12 }

  return (
    <Layout>
      <Row>
        <Col span={6} style={{ margin: "auto", minWidth: "16rem" }}>
          <Card type='inner' title='User name' bordered={false}>
            <Input
              value={username}
              onChange={handleUsername}
              size='large'
              placeholder='Enter your username'
              prefix={<UserOutlined className='site-form-item-icon' />}
            />
          </Card>
        </Col>
      </Row>

      <Row style={{ marginTop: "2rem" }}>
        <Col
          {...colSpan}
          offset={6}
          style={{ margin: "auto", minWidth: "16rem" }}
        >
          <CreateRoom handleCreateRoom={handleCreateRoom} />
          <JoinRoom handleJoinRoom={handleJoinRoom} />
        </Col>
      </Row>
    </Layout>
  )

}

export default Lobby
