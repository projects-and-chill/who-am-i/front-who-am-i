import React, { useEffect } from "react"
import { Button } from "antd"
import { PoweroffOutlined } from "@ant-design/icons"
import { usePlayers } from "../../../../../contexts/Players/PlayersContext"
import { useMe } from "../../../../../contexts/Me/MeContext"
import { useSocketIo } from "../../../../../contexts/SocketIo/SocketIoContext"
import { roomEvents } from "../../../../../_definitions/Events"
import { useRoom } from "../../../../../contexts/Room/RoomContext"
import WordInput from "./WordInput/WordInput"
import { IRoom } from "../../../../../_definitions/IRoom"
import { ITeam } from "../../../../../_definitions/ITeam"
import { updateAllTeams } from "../../../../../contexts/Teams/TeamsActionCreator"
import { useTeams } from "../../../../../contexts/Teams/TeamsContext"
import { useNavigate } from "react-router-dom"
import { IChrono } from "../../../../../_definitions/IChrono"
import { useChrono } from "../../../../../contexts/Chrono/ChronoContext"

const PlayButton = () => {

  const navigate = useNavigate()
  const { socket } = useSocketIo()
  const { players } = usePlayers()
  const { me } = useMe()
  const { setChrono } = useChrono()
  const { room, setRoom } = useRoom()
  const { setTeams } = useTeams()

  useEffect(() => {

    socket
      .on(roomEvents.server.CLOSED, (room: IRoom) => {
        setRoom(room)
      })
      .on(roomEvents.server.GAME_STARTED, (room: IRoom, teams: ITeam[], chrono: IChrono) => {
        setRoom(room)
        setTeams(updateAllTeams(teams))
        setChrono(chrono)
      })

    return () => {
      socket
        .off(roomEvents.server.CLOSED)
        .off(roomEvents.server.GAME_STARTED)
    }

  }, [navigate, setChrono, setRoom, setTeams, socket])

  const handleCloseRoom = () => {
    socket.emit(roomEvents.client.CLOSE, room.id)
  }

  return (
    <section>
      {room.open ? (
        <Button
          style={{ width: "fit-content" }}
          onClick={handleCloseRoom}
          type='primary'
          icon={<PoweroffOutlined />}
          loading={players.length <= 1}
          disabled={!me.leader && players.length > 1}
        >
          {players.length <= 1 ? "Waiting more players" : "Play"}
        </Button>
      ) : (
        <WordInput />
      )}
    </section>
  )

}

export default PlayButton
