import { Form, InputNumber, Select } from "antd"
import React, { FC, useState } from "react"
import { IRules } from "../../../../../../_definitions/IRules"
import { Rule } from "rc-field-form/lib/interface"

const { Option } = Select
type TimeType = IRules["time"][0]
interface TimeValue {
  time: number
  type: TimeType
}
type rulesChangeEmitter = (rule: Partial<IRules>) => void
interface TimeInputProps {
  value?: TimeValue
  onChange?: (value: TimeValue) => void
  emitRuleChange: rulesChangeEmitter
}
const timeLimits = {
  min: 5,
  max: 3600
}

const TimeInput: FC<TimeInputProps> = ({
  value = {
    time: timeLimits.min,
    type: "countdown"
  },
  onChange,
  emitRuleChange
}) => {

  const [clicsTimeout, setClicsTimeout] = useState<NodeJS.Timeout>()
  const triggerChange = (changedValue: Partial<TimeValue>): TimeValue => {

    const newValue: Required<TimeValue> = {
      ...value,
      ...changedValue
    }
    onChange?.(newValue)
    return newValue

  }

  const onTimeChange = (newTime: number) => {
    if (Number.isNaN(newTime))
      return
    triggerChange({ time: newTime })

    let updateField = triggerChange({ time: newTime })

    if (clicsTimeout)
      clearTimeout(clicsTimeout)

    const newTimer = setTimeout(() => {

      const { min, max } = timeLimits
      if (newTime < min)
        updateField = triggerChange({ time: min })
      else if (newTime > max)
        updateField = triggerChange({ time: max })

      const { time, type } = updateField

      emitRuleChange({ time: [type, time] })

    }, 500)
    setClicsTimeout(newTimer)
  }

  const onTimeTypeChange = (newType: TimeType) => {
    const { time, type } = triggerChange({ type: newType })
    emitRuleChange({ time: [type, time] })
  }

  const typeSelect = (
    <Select
      value={value.type}
      style={{ width: "fit-content", minWidth: "7rem" }}
      onChange={onTimeTypeChange}
    >
      <Option value='stopwatch'>StopWatch</Option>
      <Option value='countdown'>CountDown</Option>
    </Select>
  )

  return (
    <span>
      {value?.type === "countdown"
        ? <InputNumber
          type='number'
          min={timeLimits.min}
          step={5}
          value={value.time}
          onChange={onTimeChange}
          addonBefore={typeSelect}
          style={{ width: "12rem" }}
        />
        : <> {typeSelect}</>
      }

    </span>
  )

}

/*
* -
* -
* -
* -*/

type TimeFieldProps = {
  name: string
  emitRuleChange: rulesChangeEmitter
}
const TimerField: FC<TimeFieldProps> = ({ name, emitRuleChange }) => {

  const checkTimer = (_: Rule, value: Required<TimeValue>) => {

    const { time } = value
    const { min, max } = timeLimits
    if (time < min || time > max) {
      return Promise.reject(
        new Error(`Time quantity must be between ${min} and ${max}`)
      )
    }
    return Promise.resolve()

  }

  return (
    <Form.Item
      required={true}
      name={name}
      label='Time'
      rules={[{ validator: checkTimer }]}
    >
      <TimeInput emitRuleChange={emitRuleChange} />
    </Form.Item>
  )

}

export default TimerField
