import React, { memo } from "react"
import { useRoom } from "../../../../contexts/Room/RoomContext"
import Teams from "./Teams/Teams"
import Rules from "./Rules/Rules"
import PlayButton from "./PlayButton/PlayButton"

const WaitingRoom = memo(() => {

  const { room } = useRoom()

  return (
    <>
      <div>
        <img src={room.avatar + "?size=60"} alt={""} />
        <h1>{room.name}</h1>
      </div>
      <Teams />
      <br />
      <Rules />
      <br />
      <PlayButton />
    </>
  )

})

WaitingRoom.displayName = "Room"

export default WaitingRoom
