import React, { CSSProperties, memo, useCallback, useMemo } from "react"
import { useTeams } from "../../../../../../contexts/Teams/TeamsContext"
import { ITeam } from "../../../../../../_definitions/ITeam"
import { useRoom } from "../../../../../../contexts/Room/RoomContext"
import PlayersByTeam from "./NestedList/NestedList"

function PlayersListByTeam () {

  const { teams } = useTeams()
  const { room } = useRoom()

  const teamTurn : ITeam| undefined = useMemo(()=>{
    return teams.find(team =>team.id === room.teamTurn)
  }, [teams, room.teamTurn])

  const teamsColors : Record<string, string> = useMemo(()=>{
    const teamColors : Record<string, string> = {}
    teams.forEach(team => {
      teamColors[team.id] = team.color
    })
    return teamColors
  }, [teams])

  const styleTransform = useCallback((teamId: string) : CSSProperties =>{

    let cssProperties : CSSProperties = {}
    if (teamId != teamTurn?.id)
      return cssProperties
    cssProperties = {
      transform: "scale(1.08) translateX(2%)",
      boxShadow: "0px 25px 40px -10px rgba(0, 0, 0, 0.27)"
    }
    return cssProperties
  }, [teamTurn?.id])

  const teamOrder : ITeam[] = useMemo(()=>{
    return teams.sort((a, b) => a.playOrder < b.playOrder ? -1 : 1)
  }, [teams])

  return (
    <div style={{ maxWidth: "20rem", minWidth: "15rem" }} >
      <h3>Players</h3>
      {
        teamOrder.map((team) => (
          <div key={team.id} style={{
            ...styleTransform(team.id),
            transition: "2s transform",
            backgroundColor: "white",
            border: `2px solid ${teamsColors[team.id]}`,
            borderRadius: "10px"
          }}>
            <PlayersByTeam teamsColors={teamsColors} teamId={team.id} teamTurn={teamTurn} />
          </div>
        ))
      }
    </div>
  )

}

export default memo(PlayersListByTeam)
