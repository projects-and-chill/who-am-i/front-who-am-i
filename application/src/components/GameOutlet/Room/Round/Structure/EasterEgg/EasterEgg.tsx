import React, { memo, useState } from "react"
import { message } from "antd"

const EasterEgg = memo<{ myTurn : boolean, children?: JSX.Element}>(
  ({ myTurn, children }) => {

    const [timer, setTimer] = useState<NodeJS.Timeout>()
    const [clickCount, setClickCount] = useState<number>(0)

    const handleEasterEgg = ()=> {
      if (!myTurn)
        return

      setClickCount(prevCount => prevCount + 1)

      if (clickCount >= 3) {
        message.warning(
          <div>
            <p>"Calm down !!"</p>
            <img alt={""} src="https://media.giphy.com/media/xxhKYiOOIs9mGZz1Hy/giphy.gif" width="250" />
          </div>
        )
        setClickCount(0)
      }

      if (timer)
        clearTimeout(timer)

      const newTimer = setTimeout(() => {
        if (clickCount)
          setClickCount(0)
      }, 500)
      setTimer(newTimer)
    }

    return (
      <div onClick={handleEasterEgg}>
        {children}
      </div>
    )
  })
EasterEgg.displayName = "EasterEgg"

export default EasterEgg
