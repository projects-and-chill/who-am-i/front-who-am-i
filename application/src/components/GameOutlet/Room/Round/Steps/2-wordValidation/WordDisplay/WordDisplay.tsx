import React, { FC, memo, useMemo } from "react"
import { IWord } from "../../../../../../../_definitions/IWord"
import { useMe } from "../../../../../../../contexts/Me/MeContext"
import { Col, Row } from "antd"
import MulticolorTitle from "../../../../../../Structure/MulticoloreTitle/MulticolorTitle"
import TypingGradient from "../../../../../../Structure/TypingGradient/TypingGradient"

type WordValidationProps = {
  myTurn : boolean,
  word : IWord|undefined,
}

const WordDisplay : FC<WordValidationProps> = ({ myTurn, word }) => {

  const { me } = useMe()

  const canSeeWord : boolean = useMemo(() => !(me.blinded && myTurn),
    [me.blinded, myTurn])

  return (
    <div>
      <Row>
        <Col span={5} style={{ minWidth: "9rem" }} >
          {word?.validate === 1 && <MulticolorTitle content={["Keep with ", "same", " word ?"]} />}
          {word?.validate === 0 && <MulticolorTitle content={["Vote", "for the", "word !"]} />}
        </Col >

        <Col offset={2}>
          {word &&
            <div>
              {canSeeWord
                ? <TypingGradient content={["Hi, the word is", (word.value)]} />
                : <TypingGradient content={["You can't see word", ""]} />
              }
            </div>
          }
        </Col>

      </Row>
    </div>
  )
}

export default memo(WordDisplay)
