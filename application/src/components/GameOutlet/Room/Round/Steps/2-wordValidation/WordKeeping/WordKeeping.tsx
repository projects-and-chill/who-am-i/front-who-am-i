import React from "react"
import { Button, message } from "antd"
import { useMe } from "../../../../../../../contexts/Me/MeContext"
import { ITeam } from "../../../../../../../_definitions/ITeam"
import { FC } from "react"
import { roomEvents, wordEvent } from "../../../../../../../_definitions/Events"
import { useSocketIo } from "../../../../../../../contexts/SocketIo/SocketIoContext"

type WordValidationProps = {
  myTurn : boolean,
  teamTurn?: ITeam
}

const WordKeeping : FC<WordValidationProps> = ({ myTurn }) => {

  const { me } = useMe()
  const { socket } = useSocketIo()

  const handleKeepWord = () => {
    if (!myTurn)
      return message.warning("It's not your turn")
    socket.emit(roomEvents.client.NEXT_STEP, ...[me.roomId])
  }

  const handleChangeWord = () => {
    if (!myTurn && !me.leader)
      return message.warning("you cannot change word")
    socket.emit(wordEvent.client.CHANGE, ...[me.teamId, me.roomId])
  }

  return (
    <div>
      <Button disabled={!myTurn} onClick={handleKeepWord}> Keep word </Button>
      <br/>
      <Button disabled={!me.leader} onClick={handleChangeWord}> {me.leader ? "Change word (leader)" : "Change word (leader only)"} </Button>
    </div>
  )
}

export default WordKeeping
