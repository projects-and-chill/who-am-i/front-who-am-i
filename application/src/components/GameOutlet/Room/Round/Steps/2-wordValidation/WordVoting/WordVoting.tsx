import React, { CSSProperties, FC, useEffect, useMemo } from "react"
import { Avatar, message } from "antd"
import Ribbon from "antd/es/badge/Ribbon"
import { usePlayers } from "../../../../../../../contexts/Players/PlayersContext"
import { useSocketIo } from "../../../../../../../contexts/SocketIo/SocketIoContext"
import { useMe } from "../../../../../../../contexts/Me/MeContext"
import { IPlayer } from "../../../../../../../_definitions/IPlayer"
import { playerEvents } from "../../../../../../../_definitions/Events"
import { updateAllPlayers, updatePlayer } from "../../../../../../../contexts/Players/PlayersActionCreator"
import styleModule from "./WordVoting.module.css"
import { CheckOutlined, CloseOutlined } from "@ant-design/icons"
import { MeServices } from "../../../../../../../contexts/Me/fetchMe"
import { ITeam } from "../../../../../../../_definitions/ITeam"
import EasterEgg from "../../../Structure/EasterEgg/EasterEgg"

type Message = {
  type: "error" | "success",
  content: string
}

type WordValidationProps = {
  myTurn : boolean,
  teamTurn : ITeam,
}

const WordVoting : FC<WordValidationProps> = ({ myTurn, teamTurn }) => {

  const { socket } = useSocketIo()
  const { setPlayers, players } = usePlayers()
  const { me, setMe } = useMe ()

  useEffect(()=>{
    socket
      .on(playerEvents.server.VOTED, (player: IPlayer)=> {
        setPlayers(updatePlayer(player))
        MeServices.fetchMe(socket.id, player, setMe)
      })
      .on(playerEvents.server.RESET_VOTE, (players: IPlayer[], msg?: Message)=> {
        if (msg)
          message[msg.type](msg.content.toUpperCase())
        setPlayers(updateAllPlayers(players))
        MeServices.fetchMe(socket.id, players, setMe)
      })

    return ()=> {
      socket
        .off(playerEvents.server.VOTED)
        .off(playerEvents.server.RESET_VOTE)
    }

  }, [setMe, setPlayers, socket])

  const canSeeWord : boolean = useMemo(() => {
    return !(me.blinded && myTurn)
  },
  [me.blinded, myTurn])

  const handleVote = (vote: 1 | -1) => {
    if (me.vote === vote)
      return message.warning("You already vote this")
    else if (!canSeeWord)
      return message.warning("You can't vote! You are blinded")

    if (!teamTurn)
      return message.warning("It's not time to vote")

    socket.emit(playerEvents.client.VOTE_WORD, ...[vote, teamTurn.wordId, me.roomId])
  }

  const playerRibbon = (player: IPlayer) => {
    const ribbon = {
      text: "",
      color: "grey"
    }

    if (player.vote === 1) {
      ribbon.color = "green"
      ribbon.text = "Ooookkk !"
    }
    if (player.vote === -1) {
      ribbon.color = "red"
      ribbon.text = "Nopeee"
    }
    return ribbon
  }

  const buttonStyle : CSSProperties = useMemo(()=>{
    const cssProperties : CSSProperties = canSeeWord
      ? {} : {
        backgroundColor: "lightgrey",
        userSelect: "none",
        cursor: "not-allowed"
      }
    return cssProperties
  }, [canSeeWord])

  return (
    <div>

      <section style={{ display: "flex", justifyContent: "space-evenly" }}>
        {players.map(player => (
          <div key={player.id} style={{ width: "9rem" }}>
            <Ribbon placement="start" {...playerRibbon(player)} >
              <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                <Avatar size={100} src={player.avatar} />
                <div>{player.username}</div>
              </div>
            </Ribbon>
          </div>
        ))}
      </section>

      <br/>

      <EasterEgg myTurn={myTurn} >
        <div className={styleModule["page-wrapper"]}>

          <div onClick={()=> handleVote(1)} className={styleModule["circle-wrapper"]} >
            <div style={ buttonStyle } className={`${styleModule["success"]} ${styleModule["circle"]}`}></div>
            <div className={styleModule["icon"]}>
              <CheckOutlined />
            </div>
          </div>

          <div onClick={()=> handleVote(-1)} className={styleModule["circle-wrapper"]}>
            <div style={ buttonStyle } className={`${styleModule["error"]} ${styleModule["circle"]}`}></div>
            <div className={styleModule["icon"]}>
              <CloseOutlined />
            </div>
          </div>

        </div>
      </EasterEgg>

    </div>
  )
}

export default WordVoting
