import React, { useEffect } from "react"
import CountDownTimer from "./CountDownTimer/CountDownTimer"
import { useChrono } from "../../../../../../../contexts/Chrono/ChronoContext"
import StopWatchTimer from "./StopWatchTimer/StopWatchTimer"
import { message, Segmented } from "antd"
import { IChrono } from "../../../../../../../_definitions/IChrono"
import { useSocketIo } from "../../../../../../../contexts/SocketIo/SocketIoContext"
import { chronoEvents } from "../../../../../../../_definitions/Events"

const Chrono = () => {

  const { chrono, setChrono } = useChrono()
  const { socket } = useSocketIo()

  useEffect(()=> {
    socket.on(chronoEvents.server.TYPE_CHANGE, (chrono: IChrono)=> {
      setChrono(chrono)
    })

    return ()=> {
      socket.off(chronoEvents.server.TYPE_CHANGE)
    }
  })

  const handleToggleChronoType = (value: string|number) => {
    const types : IChrono["type"][] = ["countdown", "stopwatch"]
    if (!types.find(type => type === value))
      message.warning("You should select a correct type")
    socket.emit(chronoEvents.client.CHANGE_TYPE, ... [value, chrono.id, chrono.roomId])
  }

  return (
    <section style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
      <div style={{ marginBottom: "1rem", marginRight: "5rem" }}>
        <Segmented
          value={chrono.type}
          options={[{
            label: "CountDown",
            value: "countdown"
          },
          {
            label: "StopWatch",
            value: "stopwatch"
          }]}
          onChange={handleToggleChronoType}
        />
      </div>
      <div>
        {chrono.type === "countdown" ? <CountDownTimer /> : <StopWatchTimer />}
      </div>
    </section>
  )
}

export default Chrono
