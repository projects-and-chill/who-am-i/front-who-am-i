import React, { useMemo } from "react"
import Steps from "./Steps/Steps"
import { Col, Row, Tag } from "antd"
import { useRoom } from "../../../../contexts/Room/RoomContext"
import MulticolorTitle from "../../../Structure/MulticoloreTitle/MulticolorTitle"
import { useTeams } from "../../../../contexts/Teams/TeamsContext"
import PlayersListByTeam from "./Structure/PlayersListByTeam/PlayersListByTeam"
import ScoreTable from "./Structure/ScoreTable/ScoreTable"

const Round = () => {

  const { room } = useRoom()
  const { teams } = useTeams()

  const teamTurn = useMemo(()=>{
    return teams.find(team =>team.id === room.teamTurn)
  }, [teams, room.teamTurn])

  const colSpan = { lg: 24, xl: 22, xxl: 20 }

  return (
    <Col {...colSpan} style={{ margin: "auto" }}>

      <Row style={{ marginBottom: "3rem" }}>
        <Col>
          <MulticolorTitle style={{ fontSize: "2rem" }} content={["Round", room.round?.toString()]} />
          <h2>Team  <Tag color={teamTurn?.color}>{teamTurn?.id}</Tag> </h2>
        </Col>
      </Row>

      <Row style={{
        marginBottom: "3rem",
        columnGap: "4rem",
        rowGap: "3rem"

      }}>

        <Col >
          <PlayersListByTeam />
        </Col>

        <Col flex="1 1 430px" >
          <Steps />
        </Col>

      </Row>

      <Row style={{ marginBottom: "3rem" }} >
        <Col style={{ width: "100%" }}>
          <ScoreTable />
        </Col>
      </Row>

    </Col>

  )

}

export default Round
