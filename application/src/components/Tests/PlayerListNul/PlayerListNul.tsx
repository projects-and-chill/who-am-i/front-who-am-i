import React, { CSSProperties, FC, useCallback, useMemo } from "react"
import { usePlayers } from "../../../contexts/Players/PlayersContext"
import { Avatar, List, Tag } from "antd"
import { roomTemplate, useRoom } from "../../../contexts/Room/RoomContext"
import { useTeams } from "../../../contexts/Teams/TeamsContext"
import { CrownOutlined } from "@ant-design/icons"
import { ITeam } from "../../../_definitions/ITeam"
import { IPlayer } from "../../../_definitions/IPlayer"
import { IRoom } from "../../../_definitions/IRoom"

const fakePlayers : IPlayer[] = [
  {
    id: "p-rthgzgr",
    blinded: true,
    teamId: "t-zfjftyj",
    username: "enzo",
    avatar: "https://avatars.dicebear.com/api/adventurer/your-custom-seed.svg",
    vote: 0,
    roomId: "r-ytrfgooij"
  },
  {
    id: "p-thhgzgr",
    blinded: false,
    teamId: "t-zfjftyj",
    username: "angela",
    avatar: "https://avatars.dicebear.com/api/adventurer/your-seed.svg",
    vote: 0,
    roomId: "r-ytrfgooij"
  },
  {
    id: "p-gerqger",
    blinded: false,
    teamId: "t-iiiiii",
    username: "wilson",
    avatar: "https://avatars.dicebear.com/api/adventurer/yoergervg.svg",
    vote: 0,
    roomId: "r-ytrfgooij"
  },
  {
    id: "p-ergerrg",
    blinded: false,
    teamId: "t-iiiiii",
    username: "Guillaume",
    avatar: "https://avatars.dicebear.com/api/adventurer/yoezferergergervg.svg",
    vote: 0,
    roomId: "r-ytrfgooij"
  }
]

const fakeTeams : ITeam[] = [
  {
    id: "t-zfjftyj",
    wordId: "",
    color: "#f59e42",
    score: 2,
    played: false,
    playOrder: 1,
    guessedWords: [],
    roomId: "r-ytrfgooij"
  },
  {
    id: "t-iiiiii",
    wordId: "",
    color: "#42f557",
    score: 1,
    played: false,
    playOrder: 2,
    guessedWords: [],
    roomId: "r-ytrfgooij"
  }
]

const fakeRoom : IRoom = {
  ...roomTemplate,
  id: "r-ytrfgooij",
  teamTurn: "t-zfjftyj",
  leader: "p-rthgzgr"
}

const PlayerListNul : FC = () => {

  const room = fakeRoom
  const teams = fakeTeams
  const players = fakePlayers

  const teamTurn : ITeam| undefined = useMemo(()=>{
    return teams.find(team =>team.id === room.teamTurn)
  }, [teams, room.teamTurn])

  const roomLeader = useCallback(
    (playerId: string) => {
      if (playerId === room.leader)
        return <CrownOutlined style={{ color: "#f0aa13" }} />
      return ""
    },
    [room.leader]
  )

  const teamsColors : Record<string, string> = useMemo(()=>{
    const teamColors : Record<string, string> = {}
    teams.forEach(team => {
      teamColors[team.id] = team.color
    })
    return teamColors
  }, [teams])

  const styleTransform = useCallback((teamId: string) : CSSProperties =>{

    let cssProperties : CSSProperties = {}
    if (teamId != teamTurn?.id)
      return cssProperties
    cssProperties = {
      transform: "scale(1.3)",
      boxShadow: ` 2.8px 2.8px 2.2px rgba(0, 0, 0, 0.02),
  6.7px 6.7px 5.3px rgba(0, 0, 0, 0.028),
  12.5px 12.5px 10px rgba(0, 0, 0, 0.035),
  22.3px 22.3px 17.9px rgba(0, 0, 0, 0.042),
  41.8px 41.8px 33.4px rgba(0, 0, 0, 0.05),
  100px 100px 80px rgba(0, 0, 0, 0.07)
      `,
      border: "solid 1px grey"
    }
    return cssProperties
  }, [teamTurn?.id])

  return (
    <div style={{ maxWidth: "20rem" }}>
      <List
        size="small"
        header={<h3>Players</h3>}
        dataSource={players}
        bordered
        renderItem={player =>(
          <List.Item style={{ borderTop: "1px solid lightgrey"/* ...styleTransform(player.teamId)*/ }}>
            <div>
              <Avatar src={player.avatar} />
              <span style={{ marginLeft: "1rem", color: teamsColors[player.teamId] }} >
                {player.username}
              </span>
            </div>

            <div>
              {roomLeader(player.id)}
            </div>
            <div>{(player.blinded && (player.teamId === teamTurn?.id)) &&
              <Tag color="geekblue">Blinded</Tag>
            }</div>
          </List.Item>
        )} />
    </div>

  )
}

export default PlayerListNul
