import React, { memo, useCallback, useState } from "react"

const HypperVitesse = memo(function ({ handleClick }: any) {

  console.log("not re-render: HYPER-VITESSE")
  return <p style={{ border: "2px blue solid" }}> Memoized text </p>

})
HypperVitesse.displayName = "HypperVitesse"

/*
 *
 *
 *
 *
 * */
const NotMemoized = () => {

  console.log("re-render: NOT-MEMOIZED")
  return <p style={{ border: "2px red solid" }}> Not memoized text</p>

}
/*
 *
 *
 *
 *
 * */
const Memoization = () => {

  const [count, setCount] = useState(1)

  const handleClick = useCallback(() => {

    alert("yo")

  }, [])

  return (
    <div>
      <HypperVitesse handleClick={handleClick}> </HypperVitesse>
      <NotMemoized />
      <button onClick={() => setCount((c) => c + 1)}> increment {count}</button>
    </div>
  )

}

export default Memoization
