import React, { ChangeEvent, memo, useState } from "react"
import { Card, Input } from "antd"
import { debug as dbg } from "debug"

const { Search } = Input

type CreateRoomProps = {
  username: string
}

const debug = dbg("FakeComponent")

function FakeComponent ({ username }: CreateRoomProps) {

  debug("%s", "Render")
  const [roomName, setRoomName] = useState<string>("")

  const handleRoomName = (e: ChangeEvent<HTMLInputElement>) => {

    setRoomName(e.target.value)

  }

  return (
    <Card type='inner' title='Create room' bordered={false}>
      <h1>Hey {username}</h1>
      <Search
        value={roomName}
        placeholder='Enter room name'
        allowClear={false}
        enterButton='Create room'
        size='large'
        onChange={handleRoomName}
      />
    </Card>
  )

}

export default memo(FakeComponent)
// export default FakeComponent
