import React, { FC, useEffect, useMemo, useState } from "react"
import { Button, Progress } from "antd"
import { MinusOutlined, PlusOutlined } from "@ant-design/icons"

type TimerState = "pause" | "play"
type CountDownProps = {
  time: number
}

const CountDownTimerVanilla : FC<CountDownProps> = ({ time }) => {

  const [timer, setTimer] = useState<NodeJS.Timer>()
  const [seconds, setSeconds] = useState<number>(0)
  const [timerState, setTimerState] = useState<TimerState>("pause")

  useEffect(()=>{
    setSeconds(time)
  }, [time])

  useEffect(()=>{
    return ()=> clearInterval(timer)
  }, [timer])

  const handleUpdateTime = (seconds: number) => {
    setSeconds(prevSeconds => prevSeconds + seconds)
  }

  const handlePlay = () =>{
    const newTimer = setInterval(()=>{
      setSeconds(prevTime => prevTime - 1)
    }, 1000)
    setTimer(newTimer)
    setTimerState("play")
  }

  const handlePause = () =>{
    clearInterval(timer)
    setTimerState("pause")
  }

  const handleReset = () =>{
    clearInterval(timer)
    setSeconds(time)
    setTimerState("pause")
  }

  const percentToSecons = useMemo(() : number => {
    return seconds * 100 / time
  }, [seconds, time])

  return (
    <div>

      <div style={{ cursor: "pointer" }}>
        <Progress
          type="circle"
          width={250}
          status={"active"}
          strokeColor={{
            "0%": "#108ee9",
            "100%": "#87d068"
          }}
          format={() => `${seconds} s` }
          percent={percentToSecons}
          trailColor={"grey"}
        />
      </div>

      <Button onClick={()=> handleUpdateTime(-5)} icon={<MinusOutlined />} />
      <Button onClick={()=> handleUpdateTime(5)} icon={<PlusOutlined />} />
      <div>
        <Button onClick={handlePlay}> Play </Button>
        <Button onClick={handlePause}> Pause </Button>
        <Button onClick={handleReset}> Reset </Button>
      </div>
    </div>
  )
}

export default CountDownTimerVanilla
