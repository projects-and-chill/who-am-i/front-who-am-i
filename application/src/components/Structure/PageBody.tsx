import React, { FC, Key, ReactNode, useState } from "react"
import type { MenuProps } from "antd"
import { Layout, Menu } from "antd"
import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined
} from "@ant-design/icons"
import { Footer } from "antd/es/layout/layout"
import { Link } from "react-router-dom"

type MenuItem = Required<MenuProps>["items"][number]

const { Header, Content, Sider } = Layout

function getItem (
  label: ReactNode,
  key: Key,
  icon?: ReactNode,
  children?: MenuItem[]
): MenuItem {

  return {
    key,
    icon,
    children,
    label
  }

}

const items: MenuItem[] = [
  getItem(<Link to='game'> Game</Link>, "game", <DesktopOutlined />),
  getItem(<Link to='game'> Create game </Link>, "gameBis", <PieChartOutlined />),
  getItem("Join game", "join", <DesktopOutlined />),
  getItem("User", "sub1", <UserOutlined />, [
    getItem("Tom", "3"),
    getItem("Bill", "4"),
    getItem("Alex", "5")
  ]),
  getItem("Team", "sub2", <TeamOutlined />, [
    getItem("Team 1", "6"),
    getItem("Team 2", "8")
  ]),
  getItem(<Link to='tests'> Files </Link>, "9", <FileOutlined />)
]

type BodyProps = {
  children?: ReactNode
}

const PageBody: FC<BodyProps> = ({ children }) => {

  const [collapsed, setCollapsed] = useState(true)

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div className='logo' />
        <Menu
          theme='dark'
          defaultSelectedKeys={["1"]}
          mode='inline'
          items={items}
        />
      </Sider>

      <Layout className='site-layout'>
        <Header className='site-layout-background' style={{ padding: 0 }} />
        <Content style={{ margin: "0 16px" }}>
          <div
            className='site-layout-background'
            style={{ padding: 24, minHeight: 360 }}
          >
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Who am I ? ©2022 Created by EnzoCorp
        </Footer>
      </Layout>
    </Layout>
  )

}

export default PageBody
