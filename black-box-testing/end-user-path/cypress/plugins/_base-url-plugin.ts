import * as fs from "fs";
import path from "path";

export default (config) : void =>{
  const envFileName = "cypress.env.json"
  const envFilePath = path.resolve(process.cwd(),envFileName)

  if(!fs.existsSync(envFilePath))
    return

  const rawData = fs.readFileSync( envFilePath)
  if(!rawData)
    return

  try {
    const envFileData = JSON.parse(rawData.toString())
    if (envFileData["baseUrl"])
      config.baseUrl = envFileData["baseUrl"]
  }
  catch (err : any ){
    console.log(`The json file "${envFileName}" not well formated \n ${err.message ? "(err.message)" : "" }`)
  }

}
